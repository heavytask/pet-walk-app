import 'package:flutter/material.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/screens/become_caregiver/screen_become_caregiver.dart';
import 'package:pet_walk/screens/caregiver_job_detail/screen_caregiver_job_detail.dart';
import 'package:pet_walk/widgets/button.dart';

import 'multicheckBox.dart';

class RegistrationForm extends StatefulWidget {
  const RegistrationForm({
    Key key,
  }) : super(key: key);

  @override
  _RegistrationFormState createState() => _RegistrationFormState();
}

class _RegistrationFormState extends State<RegistrationForm> {
  var _emergencyPhoneController;

  @override
  void initState() {
    //_emergencyPhoneController = MaskedTextController(mask: '000 - 000 - 0000');
    _emergencyPhoneController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _emergencyPhoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Container(
            width: 600,
            child: Form(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ExpansionTile(
                    title: Text("Profile Info"),
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.red,
                              blurRadius: 25.0,
                              spreadRadius: 10.0,
                              offset: Offset(
                                15.0,
                                15.0,
                              ),
                            )
                          ],
                          color: Colors.white,
                          //borderRadius: BorderRadius.circular(16),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: TextFormField(
                                initialValue: "user@dummy.com",
                                enabled: false,
                                decoration: InputDecoration(
                                  labelText: "Email",
                                  labelStyle: TextStyle(
                                    color: Colors.black87,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: TextFormField(
                                initialValue: "00000000",
                                enabled: false,
                                decoration: InputDecoration(
                                  labelText: "Contact Number",
                                  labelStyle: TextStyle(
                                    color: Colors.black87,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    controller: _emergencyPhoneController,
                    autofocus: true,
                    decoration: InputDecoration(
                      labelText: "Emergency Contact Number",
                    ),
                  ),
                  MultiCheckBox(),
                  TextFormField(
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(
                      labelText: "About Experience with Pets",
                    ),
                  ),
                  TextFormField(
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(
                      labelText: "Short Description about Yourself",
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(8.0),
                    child: Button(
                      label: "Register",
                      onPressed: () {
                        Navigator.pushReplacementNamed(
                            context, BecomeCaregiverScreen.routeName);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
