import 'package:flutter/material.dart';

class MultiCheckBox extends StatefulWidget {
  const MultiCheckBox({
    Key key,
  }) : super(key: key);

  @override
  _MultiCheckBoxState createState() => _MultiCheckBoxState();
}

class _MultiCheckBoxState extends State<MultiCheckBox> {
  bool isPuppyPreferable = false, isAdultPreferable = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 16.0, bottom: 4.0),
          child: Container(
            width: double.infinity,
            child: Text(
              "Preferable Pet",
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
        ),
        CheckboxListTile(
          title: Text("Puppy"),
          value: isPuppyPreferable,
          onChanged: (bool value) {
            setState(() {
              isPuppyPreferable = value;
            });
          },
          controlAffinity: ListTileControlAffinity.leading,
        ),
        CheckboxListTile(
          title: Text("Adult"),
          value: isAdultPreferable,
          onChanged: (bool value) {
            setState(() {
              isAdultPreferable = value;
            });
          },
          controlAffinity: ListTileControlAffinity.leading,
        ),
      ],
    );
  }
}
