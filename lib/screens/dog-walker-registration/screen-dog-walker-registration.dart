import 'package:flutter/material.dart';
import 'package:pet_walk/widgets/button.dart';
import 'package:pet_walk/widgets/text_heading.dart';

import 'components/registration_form.dart';

class DogWalkerRegistrationScreen extends StatelessWidget {
  static const routeName = '/dog-registration';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextHeading(text: "Pet Walker Registration"),
              ),
              RegistrationForm(),
            ],
          ),
        ),
      ),
    );
  }
}
