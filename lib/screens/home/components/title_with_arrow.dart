import 'package:flutter/material.dart';
import 'package:pet_walk/config/constants.dart';

class TitleWithArrow extends StatelessWidget {
  final String title;

  final onPressed;

  final trailingIcon;

  const TitleWithArrow({
    Key key,
    @required this.title,
    @required this.onPressed,
    @required this.trailingIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: 16,
              color: kTextColor,
            ),
          ),
          IconButton(
            icon: trailingIcon,
            color: kPrimaryDarkColor,
            onPressed: onPressed,
          ),
        ],
      ),
    );
  }
}
