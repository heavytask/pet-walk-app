import 'package:flutter/material.dart';

import 'pet_list.dart';
import 'features.dart';
import 'intro.dart';
import 'caregiver_banner.dart';

class HomeScreenBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      child: Column(
        children: [
          IntroTextContainer(),
          BannerContainer(),
          SizedBox(
            height: 16,
          ),
          FeatureContainer(),
          PetListContainer()
        ],
      ),
    );
  }
}
