import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pet_walk/config/constants.dart';

class FeatureCard extends StatelessWidget {
  final String artwork;

  final String title;

  final onTap;

  const FeatureCard({
    Key key,
    @required this.artwork,
    @required this.title,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 230,
      margin: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: kPrimaryExtraLightColor,
            blurRadius: 4,
            spreadRadius: 2,
            offset: Offset(1, 1),
          ),
        ],
      ),
      child: Material(
        color: Colors.white,
        child: InkWell(
          splashColor: kPrimaryExtraLightColor,
          onTap: onTap,
          child: Column(
            children: [
              Container(
                width: 230,
                height: 130,
                child: SvgPicture.asset("assets/artworks/$artwork.svg"),
              ),
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        title,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          color: kPrimaryDarkColor,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
