import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/screens/user_profile_edit/screen_user_profile_edit.dart';
import 'package:pet_walk/widgets/button.dart';
import 'package:pet_walk/widgets/text_heading.dart';

class UserProfileScreen extends StatelessWidget {
  static const routeName = "/user-profile";

  String name, location, phone;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            children: [
              CircleAvatar(
                child: Icon(Icons.person),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Text(
                  "Utpal Barman",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              Text(
                "utpal@dummy.com",
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("4.5"),
                  Icon(
                    Icons.star,
                    color: Colors.orange,
                  ),
                ],
              ),
              SizedBox(height: 20),
              Divider(),
              SizedBox(height: 20),
              ListTile(
                leading: Icon(Icons.phone),
                title: Text("000 - 000 - 0000"),
              ),
              ListTile(
                leading: Icon(Icons.location_on),
                title: Text("2122 Street Name, Dallas, TX"),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                width: double.infinity,
                padding: const EdgeInsets.all(8.0),
                child: Button(
                  label: "Edit Profile",
                  onPressed: () {
                    Navigator.pushNamed(
                        context, UserProfileEditScreen.routeName);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
