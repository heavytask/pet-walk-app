import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_walk/cubit/pet_list_cubit.dart';
import 'package:pet_walk/models/pet_model.dart';
import 'package:pet_walk/widgets/button.dart';
import 'package:pet_walk/widgets/text_heading.dart';

class PetAddScreen extends StatelessWidget {
  static const routeName = '/pet-add';

  String petName;
  String petBirthYear;
  String petShortDescription;

  var _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextHeading(
                  text: "Add New Pet",
                ),
              ),
              Container(
                width: double.infinity,
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Container(
                      width: 600,
                      child: Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            TextFormField(
                              autofocus: true,
                              decoration: InputDecoration(
                                labelText: "Pet Name",
                              ),
                              onSaved: (newValue) => petName = newValue,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                labelText: "Birth Year",
                              ),
                              onSaved: (newValue) => petBirthYear = newValue,
                            ),
                            TextFormField(
                              minLines: 1,
                              maxLines: 3,
                              decoration: InputDecoration(
                                labelText: "Short Description about Pet",
                              ),
                              onSaved: (newValue) =>
                                  petShortDescription = newValue,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: double.infinity,
                              padding: const EdgeInsets.all(8.0),
                              child: BlocBuilder<PetListCubit, PetListState>(
                                builder: (context, state) {
                                  return Button(
                                    label: "Add to Pet List",
                                    onPressed: () {
                                      _formKey.currentState.save();
                                      context.bloc<PetListCubit>().addToPetList(
                                          Pet(
                                              name: petName,
                                              birthYear: petBirthYear,
                                              aboutPet: petShortDescription));
                                      Navigator.pop(context);
                                    },
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
