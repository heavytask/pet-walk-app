import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pet_walk/screens/caregiver_job_detail/screen_caregiver_job_detail.dart';
import 'package:pet_walk/widgets/text_heading.dart';

class BecomeCaregiverScreen extends StatelessWidget {
  static const routeName = '/become-caregiver';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextHeading(
                text: "Become Caregiver",
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Requested Jobs around you"),
            ),
            Divider(),
            Expanded(
              child: ListView.builder(
                itemCount: 10,
                itemBuilder: (context, index) => ListTile(
                  onTap: () {
                    Navigator.pushNamed(
                        context, CaregiverJobDetailScreen.routeName);
                  },
                  leading: FaIcon(FontAwesomeIcons.solidUserCircle),
                  title: Text("John Doe"),
                  subtitle: Text("Dallas, TX"),
                  trailing: Text("\$20"),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
