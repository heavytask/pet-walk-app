import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/cubit/cubit/selected_pets_cubit.dart';
import 'package:pet_walk/screens/requested_service/screen_requested_service.dart';
import 'package:pet_walk/widgets/button.dart';

class ServiceScreen extends StatelessWidget {
  static const routeName = '/service-screen';

  var asset;

  // const ServiceScreen({
  //   Key key,
  // }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var servicesTypeArguments = ModalRoute.of(context).settings.arguments;

    print("service screen is built...");
    var serviceType = (servicesTypeArguments as List<Map>)[0]["name"];
    print(serviceType);
    if (serviceType.contains("Pet Walk"))
      asset = "assets/artworks/dog-walk.svg";
    else if (serviceType.contains("Pet Sitting"))
      asset = "assets/artworks/good-dog.svg";
    else if (serviceType.contains("In House"))
      asset = "assets/artworks/house.svg";
    else
      asset = "assets/artworks/dog-walking.svg";
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: Container(
                width: double.infinity,
                child: FittedBox(
                    fit: BoxFit.cover, child: SvgPicture.asset(asset)),
              ),
            ),
            Container(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 10.0,
                      horizontal: 8.0,
                    ),
                    child: Center(
                      child: Text("Select a service"),
                    ),
                  ),
                  ServiceList(services: servicesTypeArguments),
                  SizedBox(
                    height: 12.0,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ServiceList extends StatefulWidget {
  const ServiceList({
    Key key,
    @required this.services,
  }) : super(key: key);

  final List<Map<String, String>> services;

  @override
  _ServiceListState createState() => _ServiceListState();
}

class _ServiceListState extends State<ServiceList> {
  int currentIndex = 0;
  var selectedService;

  @override
  void initState() {
    selectedService = widget.services[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(
          children: widget.services
              .map(
                (service) => Container(
                  color: currentIndex == widget.services.indexOf(service)
                      ? kPrimaryExtraLightColor
                      : null,
                  child: ListTile(
                    onTap: () {
                      setState(() {
                        currentIndex = widget.services.indexOf(service);
                      });

                      selectedService = widget.services[currentIndex];
                    },
                    title: Text(
                      service["name"],
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    subtitle: Text(service["desc"]),
                    trailing: Text(
                      '\$${service["price"]}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              )
              .toList(),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 12.0,
          ),
          child: BlocProvider(
            create: (context) => SelectedPetsCubit(),
            child: Row(
              children: [
                Expanded(
                  child: Button(
                    onPressed: () {
                      Navigator.pushNamed(
                          context, RequestedServiceScreen.routeName,
                          arguments: {
                            ...selectedService,
                            "date": "As soon",
                            "time": "as possible"
                          });
                    },
                    label: "Confirm Now",
                  ),
                ),
                SizedBox(width: 8),
                Container(
                  color: Color(0xfff0f0f0),
                  padding: const EdgeInsets.all(4.0),
                  child: IconButton(
                      icon: Icon(Icons.timer),
                      onPressed: () {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => Container(
                            height: 350,
                            child: ModalSheetColumn(
                              selectedService: selectedService,
                            ),
                          ),
                        ); // showModalBottomSheet

                        // Navigator.pushNamed(
                        //     context, RequestedServiceScreen.routeName,
                        //     arguments: {...selectedService, "now": false});
                      }),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class ModalSheetColumn extends StatefulWidget {
  final selectedService;

  const ModalSheetColumn({
    Key key,
    @required this.selectedService,
  }) : super(key: key);

  @override
  _ModalSheetColumnState createState() => _ModalSheetColumnState();
}

class _ModalSheetColumnState extends State<ModalSheetColumn> {
  var selectedDate = DateFormat("E, MMMM d").format(DateTime.now());
  var selectedTime = DateFormat("hh:mm a").format(DateTime.now());

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Schedule Time",
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Divider(),
          InkWell(
            onTap: () async {
              var pickedDate = await showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime.now(),
                lastDate: DateTime.now().add(
                  Duration(days: 60),
                ),
                builder: (context, child) {
                  return Theme(
                      data: ThemeData(
                        primarySwatch: createMaterialColor(kPrimaryColor),
                      ),
                      child: child);
                },
              );
              if (pickedDate != null &&
                  DateFormat("E, MMMM d").format(pickedDate) != selectedDate) {
                setState(() {
                  selectedDate = DateFormat("E, MMMM d").format(pickedDate);
                });
              }
            },
            child: Container(
              width: 360,
              color: kPrimaryExtraLightColor,
              padding: const EdgeInsets.all(16.0),
              child: Text(
                selectedDate,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Divider(),
          InkWell(
            onTap: () async {
              var pickedTime = await showTimePicker(
                context: context,
                initialTime: TimeOfDay.now(),
                builder: (context, child) {
                  return Theme(
                      data: ThemeData(
                        primarySwatch: createMaterialColor(kPrimaryColor),
                      ),
                      child: child);
                },
              );

              if (pickedTime != null) {
                setState(() {
                  selectedTime = DateFormat("hh:mm a").format(DateTime(
                      DateTime.now().year,
                      DateTime.now().month,
                      DateTime.now().day,
                      pickedTime.hour,
                      pickedTime.minute));
                });
              }
            },
            child: Container(
              width: 360,
              color: kPrimaryExtraLightColor,
              padding: const EdgeInsets.all(16.0),
              child: Text(
                selectedTime,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Divider(),
          Container(
              width: 360,
              child: Button(
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(
                        context, RequestedServiceScreen.routeName, arguments: {
                      ...widget.selectedService,
                      "date": selectedDate,
                      "time": selectedTime
                    });
                  },
                  label: "Confirm Schedule"))
        ],
      ),
    );
  }
}
