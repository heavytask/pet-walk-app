import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/cubit/cubit/selected_pets_cubit.dart';
import 'package:pet_walk/cubit/pet_list_cubit.dart';
import 'package:pet_walk/screens/success/screen_success.dart';
import 'package:pet_walk/widgets/button.dart';
import 'package:pet_walk/widgets/no_pets.dart';
import 'package:pet_walk/widgets/text_heading.dart';

import 'components/pet_list_slider.dart';

class RequestedServiceScreen extends StatelessWidget {
  static const routeName = '/request-walk-screen';
  final _petList = [
    {"name": "Spike"},
    {"name": "Bingo"},
    {"name": "Scooby"},
  ];

  @override
  Widget build(BuildContext context) {
    print("requested screen is built...");
    var receivedArguments = ModalRoute.of(context).settings.arguments as Map;
    print(receivedArguments);
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 8,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextHeading(
                text: "Booking Request",
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListTile(
                      title: Text("Type of Service"),
                      subtitle: Text(
                          '${receivedArguments["name"]}, ${receivedArguments["desc"]}'),
                      trailing: Text('\$${receivedArguments["price"]}'),
                    ),
                    ListTile(
                      title: Text("Home Address"),
                      subtitle: Text("4532 Formula Lane, Dallas, TX"),
                    ),
                    ListTile(
                      title: Text("Time & Date"),
                      subtitle: Text(
                          '${receivedArguments["date"]}, ${receivedArguments["time"]}'),
                    ),
                  ],
                ),
              ),
            ),
            BlocProvider(
              create: (context) => SelectedPetsCubit(),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: BookingCostColumn(
                    petList: _petList, receivedArguments: receivedArguments),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BookingCostColumn extends StatelessWidget {
  const BookingCostColumn({
    Key key,
    @required List<Map<String, String>> petList,
    @required this.receivedArguments,
  })  : _petList = petList,
        super(key: key);

  final List<Map<String, String>> _petList;
  final Map receivedArguments;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<PetListCubit, PetListState>(
          builder: (context, state) {
            return state.petList.length == 0
                ? Center(
                    child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: NoPetsColumn(),
                  ))
                : PetListColumn(petList: state.petList);
          },
        ),
        SizedBox(height: 4),
        Column(
          children: [
            Container(
              padding: const EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: kPrimaryExtraLightColor,
                  borderRadius: BorderRadius.circular(10)),
              child: ListTile(
                leading: Text(
                  "Total",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                trailing: BlocBuilder<SelectedPetsCubit, SelectedPetsState>(
                  builder: (context, state) {
                    var price = int.parse(receivedArguments["price"]) *
                        state.petList.length;
                    return Text(
                      '\$$price',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    );
                  },
                ),
              ),
            ),
            SizedBox(height: 8),
            Container(
                width: double.infinity,
                child: BlocBuilder<SelectedPetsCubit, SelectedPetsState>(
                  builder: (context, state) {
                    return Button(
                        onPressed: state.petList.length == 0
                            ? null
                            : () {
                                Navigator.pushReplacementNamed(
                                    context, SuccessScreen.routeName);
                              },
                        label: "Book");
                  },
                )),
            SizedBox(height: 24),
          ],
        ),
      ],
    );
  }
}
