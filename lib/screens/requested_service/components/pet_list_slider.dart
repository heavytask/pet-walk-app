import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/cubit/cubit/selected_pets_cubit.dart';
import 'package:pet_walk/models/pet_model.dart';

class PetListColumn extends StatefulWidget {
  const PetListColumn({
    Key key,
    @required List<Pet> petList,
  })  : _petList = petList,
        super(key: key);

  final List<Pet> _petList;

  @override
  _PetListColumnState createState() => _PetListColumnState();
}

class _PetListColumnState extends State<PetListColumn> {
  var selectedIndex = [];
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SelectedPetsCubit, SelectedPetsState>(
      builder: (context, state) {
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Text(
                    "Pet(s)",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                  Spacer(),
                  Text("(" + state.petList.length.toString() + ")"),
                ],
              ),
            ),
            Container(
              height: 120,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: widget._petList.length,
                  itemBuilder: (context, index) {
                    var isSelected = selectedIndex.contains(
                        widget._petList.indexOf(widget._petList[index]));

                    return Container(
                      margin: const EdgeInsets.all(8),
                      child: Material(
                        color: Colors.white,
                        child: InkWell(
                          onTap: () {
                            if (!isSelected) {
                              context
                                  .bloc<SelectedPetsCubit>()
                                  .addToSelectedPets(widget._petList[index]);
                            } else {
                              context
                                  .bloc<SelectedPetsCubit>()
                                  .removeFromSelectedPets(
                                      widget._petList[index]);
                            }

                            isSelected
                                ? selectedIndex.remove(widget._petList
                                    .indexOf(widget._petList[index]))
                                : selectedIndex.add(widget._petList
                                    .indexOf(widget._petList[index]));
                          },
                          child: Card(
                            elevation: isSelected ? 6 : 2,
                            child: Container(
                              padding: const EdgeInsets.all(8),
                              width: 120,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  isSelected
                                      ? FaIcon(
                                          FontAwesomeIcons.dog,
                                          color: kPrimaryLightColor,
                                        )
                                      : FaIcon(
                                          FontAwesomeIcons.paw,
                                          color: Colors.grey[350],
                                        ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Text(
                                    widget._petList[index].name.toString(),
                                    style: TextStyle(
                                        color: isSelected
                                            ? kPrimaryDarkColor
                                            : Colors.grey,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
            ),
          ],
        );
      },
    );
  }
}
