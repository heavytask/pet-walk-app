import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/cubit/cubit/selected_pets_cubit.dart';

class PetListColumn extends StatefulWidget {
  const PetListColumn({
    Key key,
    @required List<Map<String, String>> petList,
  })  : _petList = petList,
        super(key: key);

  final List<Map<String, String>> _petList;

  @override
  _PetListColumnState createState() => _PetListColumnState();
}

class _PetListColumnState extends State<PetListColumn> {
  var selectedIndex = [];
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Text(
                "Pet(s)",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
              Spacer(),
              Text("(2)"),
            ],
          ),
        ),
        Column(
          children: widget._petList
              .map(
                (pet) => Card(
                  child: ListTile(
                    leading: FaIcon(
                      FontAwesomeIcons.paw,
                      color:
                          selectedIndex.contains(widget._petList.indexOf(pet))
                              ? kPrimaryColor
                              : Colors.grey,
                    ),
                    title: Text(pet["name"]),
                    trailing: FaIcon(
                      FontAwesomeIcons.checkCircle,
                      color: kPrimaryColor,
                    ),
                  ),
                ),
              )
              .toList(),
        ),
      ],
    );
  }
}
