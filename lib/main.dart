import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pet_walk/config/constants.dart';
import 'package:pet_walk/screens/become_caregiver/screen_become_caregiver.dart';
import 'package:pet_walk/screens/caregiver_job_detail/screen_caregiver_job_detail.dart';
import 'package:pet_walk/screens/dog-walker-registration/screen-dog-walker-registration.dart';
import 'package:pet_walk/screens/home/screen_home.dart';
import 'package:pet_walk/screens/pet_add/screen_pet_add.dart';
import 'package:pet_walk/screens/requested_service/screen_requested_service.dart';
import 'package:pet_walk/screens/service/screen_service.dart';
import 'package:pet_walk/screens/success/screen_success.dart';
import 'package:pet_walk/screens/user_profile/screen_user_profile.dart';

import 'cubit/cubit/selected_pets_cubit.dart';
import 'cubit/pet_list_cubit.dart';
import 'screens/user_profile_edit/screen_user_profile_edit.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    print("app is built...");
    return BlocProvider(
      create: (context) => PetListCubit(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: createMaterialColor(kPrimaryColor),
          visualDensity: VisualDensity.adaptivePlatformDensity,
          appBarTheme: kAppBarTheme,
          textTheme: GoogleFonts.muliTextTheme(),
          scaffoldBackgroundColor: kBackgroundColor,
        ),
        home: BlocProvider(
          create: (context) => SelectedPetsCubit(),
          child: HomeScreen(),
        ),
        routes: {
          '/service-screen': (context) => ServiceScreen(),
          '/request-walk-screen': (context) => RequestedServiceScreen(),
          '/become-caregiver': (context) => BecomeCaregiverScreen(),
          '/caregiver-job-detail': (context) => CaregiverJobDetailScreen(),
          '/user-profile': (context) => UserProfileScreen(),
          '/user-profile-edit': (context) => UserProfileEditScreen(),
          '/success': (context) => SuccessScreen(),
          '/dog-registration': (context) => DogWalkerRegistrationScreen(),
          '/pet-add': (context) => PetAddScreen(),
        },
      ),
    );
  }
}
