import 'package:flutter/material.dart';
import 'package:pet_walk/config/constants.dart';

class TextHeading extends StatelessWidget {
  final String text;

  const TextHeading({
    Key key,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 24,
        color: kPrimaryDarkColor,
      ),
    );
  }
}
