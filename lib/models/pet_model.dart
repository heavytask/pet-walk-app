class Pet {
  final String name;
  final String birthYear;
  final String aboutPet;

  Pet({
    this.name,
    this.birthYear,
    this.aboutPet,
  });
}
